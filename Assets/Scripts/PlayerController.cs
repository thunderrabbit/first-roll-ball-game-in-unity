﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	private int jewelPickupCount;
	public Text winText;
	public Text countText;
	public float speed;		// public allows edit in editor

	void SetCountText(int count) {
		countText.text = "Count: " + count.ToString ();
		winText.text = "";
	}

	void Start() {
		jewelPickupCount = 0;
		SetCountText (jewelPickupCount);
	}

	void Update() {
		// executed before rendering a frame
		// game code goes here
	}

	void FixedUpdate() {
		// executed before physics calculations
		// physics code goes here
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0, moveVertical);

		rigidbody.AddForce (movement * speed * Time.deltaTime);
	}

	void OnTriggerEnter(Collider other) {
		// Destroy (other.gameObject);
		if (other.gameObject.tag == "Jewel") {
			other.gameObject.SetActive (false);
			jewelPickupCount++;
			SetCountText (jewelPickupCount);
			if(jewelPickupCount >= 11) {
				winText.text = "You win!!";
			}
		}
	}
}
