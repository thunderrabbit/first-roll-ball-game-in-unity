﻿using UnityEngine;
using System.Collections;

public class JewelRotator : MonoBehaviour {

	int x, y, z;
	// Use this for initialization
	void Start () {
		x = Random.Range (-45, 45);
		y = Random.Range (-45, 45);
		z = Random.Range (-45, 45);
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (new Vector3 (x, y, z) * Time.deltaTime);
	}
}
