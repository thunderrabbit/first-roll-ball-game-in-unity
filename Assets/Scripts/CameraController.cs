﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public GameObject player;

	private Vector3 offset;

	// Use this for initialization
	void Start () {
		// figure out how far away we are when game starts
		offset = transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		// hover however far away we were when started
		transform.position = player.transform.position + offset;
	}
}
